﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvasiveManeuver : MonoBehaviour {

	public Vector2 startWait;
	public Vector2 maneuverTime;
	public Vector2 maneuverWait;
	public Boundary boundary;
	public float dodge;
	public float tilt;
	public float smoothing;

	private float currentSpeed;
	private float targetManeuver;
	private Rigidbody rigb;

	void Start ()
	{
		rigb = GetComponent<Rigidbody> ();
		currentSpeed = rigb.velocity.z;
		StartCoroutine (Evade ());
	}

	IEnumerator Evade ()
	{
		yield return new WaitForSeconds (Random.Range (startWait.x, startWait.y));

		while (true)
		{
			targetManeuver = Random.Range (1, dodge) * -Mathf.Sign(transform.position.x);
			yield return new WaitForSeconds (Random.Range (maneuverTime.x, maneuverTime.y));
			targetManeuver = 0;
			yield return new WaitForSeconds (Random.Range (maneuverWait.x, maneuverWait.y));
		}
			
	}
	

	void FixedUpdate ()
	{
		float newManeuver = Mathf.MoveTowards (rigb.velocity.x, targetManeuver, Time.deltaTime * smoothing);
		rigb.velocity = new Vector3 (newManeuver, 0.0f, currentSpeed);
		rigb.position = new Vector3 (Mathf.Clamp (rigb.position.x, boundary.xMin, boundary.xMax), 0.0f, Mathf.Clamp (rigb.position.z, boundary.zMin, boundary.zMax));
		rigb.rotation = Quaternion.Euler (0.0f, 0.0f, rigb.velocity.x * -tilt);
	}
}
