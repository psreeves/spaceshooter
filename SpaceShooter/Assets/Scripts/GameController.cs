﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	private bool gameOver;
	private bool restart;
    private int score;

    public Transform player;
    private float multiplier;


    void Start()
    {
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
        UpdateScore();
        StartCoroutine (SpawnWaves());
    }

	void Update()
	{
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				SceneManager.LoadScene ("Main");
			}
		}
	}

	IEnumerator SpawnWaves ()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
				GameObject hazard = hazards [Random.Range(0, hazards.Length)];
				Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

			if (gameOver)
			{
				restartText.text = "Press 'R' to Restart";
				restart = true;
				break;
			}
        }
    }

    public void AddScore (int newScoreValue)
    {
        if (((player.position.z + 4)/18) < 0.33)
        {
            multiplier = 1.0f;
        }
        if (((player.position.z + 4) / 18) >= 0.33 && ((player.position.z + 4) / 18) <= 0.66)
        {
            multiplier = 1.5f;
        }
        if (((player.position.z + 4) / 18) > 0.66)
        {
            multiplier = 2.0f;
        }

        score = Mathf.RoundToInt(score + newScoreValue * multiplier);
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

	public void GameOver()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
	}
}
