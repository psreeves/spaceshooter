﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;

}

public class PlayerController : MonoBehaviour {

    public float speed;
    public float tilt;
	public float roll;
	public float rollRate;
    public float rollTime;
    public Boundary boundary;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;

    private Rigidbody rigb;
	private AudioSource audio;
    private float nextFire;
	private float nextRoll;
    private float stopTime = -1;

	void Start () {
        rigb = GetComponent<Rigidbody> ();
		audio = GetComponent<AudioSource> ();
	}

    private void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			audio.Play ();
        }
    }
    void FixedUpdate () {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        float moveRoll = Input.GetAxis("Roll");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        if (moveRoll != 0 && Time.time > nextRoll)
        {
            stopTime = Time.time + rollTime;
            nextRoll = Time.time + rollRate;
            float rollSpeed = moveRoll * roll;
            rigb.AddForce(rollSpeed, 0.0f, 0.0f, ForceMode.Force);
        }
        if (Time.time > stopTime)
        {
            rigb.velocity = movement * speed;
        }

        rigb.position = new Vector3(Mathf.Clamp(rigb.position.x, boundary.xMin, boundary.xMax), 0.0f, Mathf.Clamp(rigb.position.z, boundary.zMin, boundary.zMax));

        rigb.rotation = Quaternion.Euler(0.0f, 0.0f, rigb.velocity.x * -tilt);
    }
}
