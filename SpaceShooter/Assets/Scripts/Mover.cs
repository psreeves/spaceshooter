﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    public float speed;

    private Rigidbody rigb;

	void Start () {

        rigb = GetComponent<Rigidbody>();
        rigb.velocity = transform.forward * speed;
	}
}
